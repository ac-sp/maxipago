<?php

 $_paths = array(
	'/var/www/vendors/',
	'/var/www/vendors.com.br/',
	'/srv/www/vendors.com.br/',
	'/wamp/www/vendors/'
);
foreach($_paths as $path) if(is_readable($path . 'detect_environment.php')) {
	$environment = require_once($path . 'detect_environment.php');	
	define('VENDORSPATH',$path);
	break;		
}

isset($environment) ? 
	(!defined('ENVIRONMENT') && define('ENVIRONMENT',$environment)) : 
	die('ambiente nao pode ser detectado.');