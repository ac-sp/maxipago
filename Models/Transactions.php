<?php

namespace acsp\maxipago\Models;

class Transactions {
    use \doctrine\Dashes\Model;
    
    public $database = 'maxipago';
    public $table = 'transactions';
    public $deactivate = false;
    public $primaryKey = 'id';
}
