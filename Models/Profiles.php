<?php

namespace acsp\maxipago\Models;

class Profiles {
    use \doctrine\Dashes\Model;
    
    public $database = 'maxipago';
    public $table = 'profiles';
    public $deactivate = false;
    public $primaryKey = 'id';
}
