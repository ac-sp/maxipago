<?php

namespace acsp\maxipago;

use \acsp\maxipago\lib\maxiPago;

class sale {

    public $maxipago;

    use \doctrine\Dashes\Model;

    public function __construct() {
        require_once 'environment.php';
        require_once VENDORSPATH . '/acsp/log/Log.php';                
        $this->maxipago = new \acsp\maxipago\lib\maxiPago;
        $this->maxipago->setCredentials($this->maxipago->lojaID, $this->maxipago->chave);
        $this->maxipago->setEnvironment($this->maxipago->env);
//        $this->maxipago->setDebug($this->maxipago->env == "TEST" ? true : false);
        $this->loadModel('\acsp\maxipago\Models\Profiles');
        $this->loadModel('\acsp\maxipago\Models\Transactions');
        if(isset($_POST['cpfcnpj'])) preg_replace("/[-\.\/]/","",$_POST['cpfcnpj']);
    }

    public function customerId() {        
        $customer = $this->model['Profiles']->find(['cpfcnpj' => $_POST['cpfcnpj']], '', '', '', '');
        \Acsp\Log::get()->add("customerId: ");
        \Acsp\Log::get()->add(var_export($customer,true));        
        if (empty($customer)) {
            $customerId = $this->addProfile();
            $profileData = ["cpfcnpj" => $_POST['cpfcnpj'], "customerId" => $customerId, "dtcreated" => date('Y-m-d H:i:s')];
            $this->model['Profiles']->save($profileData);
            \Acsp\Log::get()->add("saveProfile: ");
            \Acsp\Log::get()->add(var_export($profileData,true));
            return $customerId;
        }
        return $customer[0]['customerId'];
    }

    public function getToken() {
        $token = $this->model['Profiles']->find(['cpfcnpj' => $_POST['cpfcnpj']], '', '', '', '');   
        \Acsp\Log::get()->add("getToken: ");
        \Acsp\Log::get()->add(var_export($token,true));
        if (empty($token))
            die("token de cliente nao encontrado");
        return $token[0]['cardToken'];
    }

    public function addProfile() {
        \Acsp\Log::get()->add("addProfile: ");
        $name = explode(" ", $_POST['bname']);
        $data = array(
            "customerIdExt" => $_POST['cpfcnpj'], // REQUIRED - Customer ID created by maxiPago! after "add-customer" command //
            "firstName" => $name[0],
            "lastName" => implode(" ", array_slice($name, 1))
        );
        \Acsp\Log::get()->add(var_export($data,true));
        $this->maxipago->addProfile($data);        
        if ($this->maxipago->isErrorResponse()) {
            \Acsp\Log::get()->add(var_export(["status" => false, "msg" => $this->maxipago->getMessage()],true));
            die(json_encode(["status" => false, "msg" => $this->maxipago->getMessage()]));
        } else {            
            \Acsp\Log::get()->add("Customer id: ");
            \Acsp\Log::get()->add($this->maxipago->getCustomerId());
            return $this->maxipago->getCustomerId();
        }
    }

    public function response($id, $type, &$data) {
            \Acsp\Log::get()->add("resultado: ");
            if ($this->maxipago->isErrorResponse()) {
                \Acsp\Log::get()->add(var_export(["status" => false, "msg" => $this->maxipago->getResult()],true));
                die(json_encode(["status" => false, "msg" => $this->maxipago->getMessage()]));
            } elseif ($this->maxipago->isTransactionResponse()) {
                if ($this->maxipago->getResponseCode() == "0") {
                    if($type == "save-card") { //save card token in profiles table
                        $this->model['Profiles']->updateBy([
                            'cpfcnpj' => $_POST['cpfcnpj']],['cardToken' => "'".$this->maxipago->getToken()."'"]);
                        \Acsp\Log::get()->add("Update Profile");
                        \Acsp\Log::get()->add(var_export(['cpfcnpj' => $_POST['cpfcnpj'],['cardToken' => "'".$this->maxipago->getToken()."'"]],true));
                    }
                    $transactionData = [
                        'id' => $id, 
                        'type' => $type, 
                        'dtupdated' => date('Y-m-d H:i:s'), 
                        'transactionID' => $this->maxipago->getTransactionID(), 
                        'orderID' => $this->maxipago->getOrderID(),
                        'value' => $data['chargeTotal'],
                        'payments' => @$data['installments'],
                        'startDate' => @$data['startDate'],
                        'cpfcnpj' => @$_POST['cpfcnpj']
                    ];
                    $this->model['Transactions']->save($transactionData);
                    \Acsp\Log::get()->add("Update Transaction");
                    \Acsp\Log::get()->add(var_export($transactionData,true));
                    \Acsp\Log::get()->add(var_export(["status" => true, "result" => $this->maxipago->getResult()],true));
                    die(json_encode(["status" => true, "result" => $this->maxipago->getResult()]));
//{"status":true,"result":{"authCode":"","orderID":"0A0104A3:0163659FB8BE:9EA5:2526B4B9","referenceNum":"","transactionID":"","transactionTimestamp":"1526418356","responseCode":"0","responseMessage":"APPROVED","avsResponseCode":"","cvvResponseCode":"","processorCode":"","processorMessage":"","processorName":"SIMULATOR","creditCardBin":"491649","creditCardLast4":"1722","errorMessage":"","creditCardScheme":"Visa"}}            
                } else {
                    $transactionData = [
                        'id' => $id, 
                        'type' => $type, 
                        'dtupdated' => date('Y-m-d H:i:s'), 
                        'value' => $data['chargeTotal'],
                        'payments' => @$data['installments'],
                        'startDate' => @$data['startDate'],                        
                        'error' => $this->maxipago->getMessage()];
                    $this->model['Transactions']->save($transactionData);
                    \Acsp\Log::get()->add("Update Transaction");
                    \Acsp\Log::get()->add(var_export($transactionData,true));
                    \Acsp\Log::get()->add(var_export(["status" => false, "msg" => $this->maxipago->getResult()],true));
                    die(json_encode(["status" => false, "msg" => $this->maxipago->getMessage()]));
                }
            }        
    }
    public function direct() {     
        \Acsp\Log::get('cartaovendadireta');
        \Acsp\Log::get()->add("dados recebidos: ");
        \Acsp\Log::get()->add(var_export($_POST,true));  
        try {           
            $id = $this->model['Transactions']->save(['produto' => $_POST['produto'], 'dtcreated' => date('Y-m-d H:i:s')]);
            \Acsp\Log::get()->add("Save Transaction");
            \Acsp\Log::get()->add(var_export(['produto' => $_POST['produto'], 'dtcreated' => date('Y-m-d H:i:s')],true));            
            $type = "direct";
            $data = array(
                "processorID" => "1", // REQUIRED - Use '1' for testing. Contact our team for production values //
                "referenceNum" => "ORD" . str_pad($id, 8, "0", STR_PAD_LEFT), // REQUIRED - Merchant internal order number //
                "chargeTotal" => str_replace(",", ".", $_POST['chargeTotal']), // REQUIRED - Transaction amount in US format //
                "billingName" => strtoupper($_POST['bname']), // HIGHLY RECOMMENDED - Customer name //
                "number" => $_POST['number'], // REQUIRED - Full credit card number //
                "expMonth" => explode("/", $_POST['expiration'])[0], // REQUIRED - Credit card expiration month //
                "expYear" => "20" . explode("/", $_POST['expiration'])[1], // REQUIRED - Credit card expiration year //
                "cvvNumber" => $_POST['cvvNumber'], // HIGHLY RECOMMENDED - Credit card verification code //
            );
            if (!empty($_POST['cpfcnpj'])) { //se precisar salvar o cartao, é preciso do campo cpfcnpj
                $data["saveOnFile"] = "1";
                $data["customerId"] = $this->customerId();
                $data["onFileEndDate"] = "12/25/2999";
                $type = "save-card";
            }
            \Acsp\Log::get()->add("dados enviados");
            \Acsp\Log::get()->add(var_export($data,true));  
            $this->maxipago->creditCardSale($data);
            $this->response($id, $type, $data);           
        } catch (Exception $e) {
            \Acsp\Log::get()->add("Exception");
            \Acsp\Log::get()->add(var_export($e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine(),true));                    
            die(json_encode(['status' => false, "msg" => $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine()]));
        }
    }
    
    public function token() {
        \Acsp\Log::get('cartaovendatoken');
        \Acsp\Log::get()->add("dados recebidos: ");
        \Acsp\Log::get()->add(var_export($_POST,true));          
        try {
            $id = $this->model['Transactions']->save(['produto' => $_POST['produto'], 'dtcreated' => date('Y-m-d H:i:s')]);
            \Acsp\Log::get()->add("Save Transaction");
            \Acsp\Log::get()->add(var_export(['produto' => $_POST['produto'], 'dtcreated' => date('Y-m-d H:i:s')],true));                        
            $data = array(
                "processorID" => "1", // REQUIRED - Use '1' for testing. Contact our team for production values //
                "referenceNum" => "ORD" . str_pad($id, 8, "0", STR_PAD_LEFT), // REQUIRED - Merchant internal order number //
                "chargeTotal" => str_replace(",", ".", $_POST['chargeTotal']), // REQUIRED - Transaction amount in US format //
                "token" => $this->getToken(),
                "customerId" => $this->customerId()
            );
            \Acsp\Log::get()->add("dados enviados");
            \Acsp\Log::get()->add(var_export($data,true));                    
            $this->maxipago->creditCardSale($data);
            $this->response($id, "token", $data);          
        } catch (Exception $e) {
            \Acsp\Log::get()->add("Exception");
            \Acsp\Log::get()->add(var_export($e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine(),true));                    
            die(json_encode(['status' => false, "msg" => $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine()]));
        }
    }

    public function recurring() {       
        \Acsp\Log::get('cartaorecorrencia');
        \Acsp\Log::get()->add("dados recebidos: ");
        \Acsp\Log::get()->add(var_export($_POST,true));  
        try {
            $id = $this->model['Transactions']->save(['produto' => $_POST['produto'], 'dtcreated' => date('Y-m-d H:i:s')]);  
            \Acsp\Log::get()->add("Save Transaction");
            \Acsp\Log::get()->add(var_export(['produto' => $_POST['produto'], 'dtcreated' => date('Y-m-d H:i:s')],true));                        
            $data = array(
                "processorID" => "1", // REQUIRED - Use '1' for testing. Contact our team for production values //
                "referenceNum" => "ORD" . str_pad($id, 8, "0", STR_PAD_LEFT), // REQUIRED - Merchant internal order number //
                "chargeTotal" => str_replace(",", ".", $_POST['chargeTotal']), // REQUIRED - Transaction amount in US format //
                "number" => $_POST['number'], // REQUIRED - Full credit card number //
                "expMonth" => explode("/", $_POST['expiration'])[0], // REQUIRED - Credit card expiration month //
                "expYear" => "20" . explode("/", $_POST['expiration'])[1], // REQUIRED - Credit card expiration year //
                "cvvNumber" => $_POST['cvvNumber'], // RECOMMENDED - Credit card verification number //
                "billingName" => strtoupper($_POST['bname']), // RECOMMENDED - Customer name //
                "action" => "new",
//                "startDate" => date("Y-m-d",strtotime ( '+1 month' , strtotime ( date("Y-m-d") ) )), // REQUIRED for this command - Date of 1st payment (YYYY-MM-DD format) //
                "startDate" => date("Y-m-d"), // REQUIRED for this command - Date of 1st payment (YYYY-MM-DD format) //
                "frequency" => "1", // REQUIRED for this command - Frequency of payment (1, 3, 6, …) //
                "period" => "monthly", // REQUIRED for this command - Interval of payment: 'daily', 'weekly', 'monthly' //
                "installments" => $_POST['installments'], // REQUIRED for this command - Total number of payments before the order is completed //
                "failureThreshold" => "2" // REQUIRED for this command - Number of declines before email notification //
            );         
            \Acsp\Log::get()->add("dados enviados");
            \Acsp\Log::get()->add(var_export($data,true));                    
            $this->maxipago->createRecurring($data);
            $this->response($id, "recurring", $data);

        } catch (Exception $e) {
            \Acsp\Log::get()->add("Exception");
            \Acsp\Log::get()->add(var_export($e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine(),true));                    
            echo $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine();
        }
    }
}
